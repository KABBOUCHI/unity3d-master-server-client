﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
/// <summary>
/// Exposes easily accessible functions for the UI components to access.
/// Author: Gerard Ryan - February 2017
/// </summary>
public class UIManager : MonoBehaviour {
    MSCNetworkManager mSCNetMan;
    MSCNetworkManager MSCNetMan { get { return (mSCNetMan == null) ? mSCNetMan = FindObjectOfType<MSCNetworkManager>() : mSCNetMan; } }

    [SerializeField] Button serverPrefab;
    [SerializeField] Text infoField;
    [SerializeField] RectTransform serverArea;
    [SerializeField] InputField mSPortInput;
    [SerializeField] InputField serverInfoInput;
    [SerializeField] Toggle makePublicInput;
    float nextServer = 0;

    public void Update() {
        infoField.text = (MSCNetMan.MSClient.ErrorDescription != "")? MSCNetMan.MSClient.ErrorDescription : infoField.text;
    }

    public void Awake() {
        //Application.targetFrameRate = 10000;
    }

    public void GetListings() {
        RemoveListings();

        infoField.text = "Getting listings";

        // using the MSCNetworkManager to work in both scenarios
        MSCNetMan.GetServerListings((address, port, info, ping) => {
            AddListing(address, port, info, ping);
        }, (errorDesc) => { infoField.text = errorDesc; });
    }

    public void GetLANListings() {
        RemoveListings();

        infoField.text = "Getting LAN listings";

        // using the MSCNetworkManager to work in both scenarios
        MSCNetMan.GetLANServerListings((address, port, info, ping) => {
            AddListing(address, port, info, ping);
        }, (errorDesc) => { infoField.text = errorDesc; });
    }

    public void StartServer() {
        MSCNetMan.MSClient.ServerInfo = System.Text.Encoding.ASCII.GetBytes(serverInfoInput.text);
        if (MSCNetMan.MSClient.ErrorDescription != "") {
            infoField.text = MSCNetMan.MSClient.ErrorDescription;
            return;
        }

        MSCNetMan.StartServer();
    }

    public void StopServer() {
        MSCNetMan.StopServer();
    }

    public void StartMSClient() {
        UInt16 temp;
        if (UInt16.TryParse(mSPortInput.text, out temp)) {
            MSCNetMan.MSClient.NetworkPort = temp;
        }
        string errorDesc;
        if (!MSCNetMan.MSClient.Init(out errorDesc)) {
            infoField.text = errorDesc;
        } else {
            infoField.text = "Master server client started.";
            
        }
    }

    public void StopMSClient() {
        StartCoroutine(MSCNetMan.MSClient.GracefulShutdown((success, errorDesc) => {
            if (!success) {
                infoField.text = errorDesc;
            } else {
                infoField.text = "Master server client stopped.";
            }
        }));
    }

    public void MSClientServerStarted() {
        MSCNetMan.MSClient.ServerInfo = System.Text.Encoding.ASCII.GetBytes(serverInfoInput.text);
        if (MSCNetMan.MSClient.ErrorDescription != "") {
            infoField.text = MSCNetMan.MSClient.ErrorDescription;
            return;
        }

        StartCoroutine(MSCNetMan.MSClient.ServerStarted(makePublicInput.isOn, (success, message) => {
            infoField.text = message;
        }));
    }

    public void MSClientServerStopped() {
        StartCoroutine(MSCNetMan.MSClient.ServerStopped((success, errorDesc) => {
            if (!success) {
                infoField.text = errorDesc;
            } else {
                infoField.text = "Server Stopped";
            }
        }));
    }

    void AddListing(string address, UInt16 port, byte[] info, int ping) {
        Button button = Instantiate(serverPrefab);
        RectTransform buttonRect = button.GetComponent<RectTransform>();
        buttonRect.SetParent(serverArea, false);
        buttonRect.anchoredPosition = new Vector2((buttonRect.sizeDelta.x / 2) + 1, -nextServer - ((buttonRect.sizeDelta.y / 2) + 1));
        nextServer += buttonRect.sizeDelta.y;

        int infoCharCount = 0;
        foreach (byte b in info) {
            if (b != 0) {
                infoCharCount++;
            } else {
                break;
            }
        }
        MSCServerListing connectScript = button.GetComponent<MSCServerListing>();
        connectScript.Address = address;
        connectScript.Port = port;

        button.GetComponentInChildren<Text>().text = ((infoCharCount > 0) ? System.Text.Encoding.ASCII.GetString(info, 0, infoCharCount) : " (" + address + ":" + port + ") ") + '\t' + ping + "ms";

        serverArea.sizeDelta = new Vector2(serverArea.sizeDelta.x, nextServer);
    }

    void RemoveListings() {
        while (serverArea.childCount > 0) {
            DestroyImmediate(serverArea.GetChild(0).gameObject, false);
        }
        nextServer = 0;
        serverArea.sizeDelta = new Vector2(serverArea.sizeDelta.x, nextServer);
    }
}
