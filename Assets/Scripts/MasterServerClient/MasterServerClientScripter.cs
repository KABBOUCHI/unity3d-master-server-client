﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Allows automation of a <see cref="MasterServerClient"/> to simulate use cases for testing purposes.
/// Author: Gerard Ryan - February 2017
/// </summary>
[RequireComponent(typeof(MasterServerClient))]
public class MasterServerClientScripter : MonoBehaviour {

    MasterServerClient mSClient;
    [SerializeField] UnityEngine.UI.Text logOutput;

    public string Log {
        set {
            if (logOutput == null) {
                Debug.Log(System.DateTime.Now + ": " + value);
            } else {
                logOutput.text = value;
            }
        }
    }

    System.TimeSpan startTime = System.DateTime.Now.TimeOfDay;
    float waitTime = 5;

    private bool IsStartTime() {
        return System.DateTime.Now.TimeOfDay >= startTime;
    }

    private void ParseCmdLineArgs() {
        string[] args = System.Environment.GetCommandLineArgs();

        for (int i = 1; i < args.Length; i++) {
            switch (args[i]) {
                case "-port":
                    mSClient.NetworkPort = UInt16.Parse(args[++i]);
                    break;
                case "-starttime":
                    startTime = new System.TimeSpan(int.Parse(args[++i]), int.Parse(args[++i]), int.Parse(args[++i]));
                    break;
                case "-waitseconds":
                    waitTime = float.Parse(args[++i]);
                    break;
                case "-framerate":
                    Application.targetFrameRate = int.Parse(args[++i]);
                    break;
                case "-batchmode":
                case "-nographics":
                    break;
                case "-logFile":
                    i++;
                    break;
                default:
                    Debug.LogErrorFormat("\nERROR! Unrecognised Argument: {0}", args[i]);
                    Application.Quit();
                    return;
            }
        }
    }

    void Start () {
        if (logOutput == null) {
            Debug.LogWarning("Info UI Text element is null");
        }
        mSClient = GetComponent<MasterServerClient>();
        StartCoroutine(RunScript());
    }

    private IEnumerator RunScript() {
#if !UNITY_EDITOR
        ParseCmdLineArgs(); 
#else
        mSClient.NetworkPort = 2222;
#endif
        string tmp;
        if (!mSClient.Init(out tmp))
            Log = tmp;

        // Synchronised start
        yield return new WaitUntil(() => { return IsStartTime(); });

        yield return ClientServerBrowsingAndRefresh();
    }

    private IEnumerator ClientServerBrowsingAndRefresh() {
        while (true) {
            Log = "Getting Listings...";
            mSClient.GetServerListings(null, (errorDesc) => {
                Log = errorDesc;
            });
            yield return new WaitForSeconds(waitTime);
        }
    }

    private IEnumerator StartServerAndIdle() {
        yield return mSClient.ServerStarted(true, (success, errorDesc) => {
                Log = errorDesc;
        });
    }

    private IEnumerator SimGameServerCrash() {

        Log  = "Starting server";
        bool started = false;
        yield return mSClient.ServerStarted(true, (success, message) => {
            if (started = success) {
                Log  = message;
            } else {
                Log = mSClient.NetworkPort + ": " + message;
            }
        });

        if (started) {
            yield return new WaitForSecondsRealtime(waitTime);
        } else {
            yield break;
        }

        Log = "\"Crashing\"";
        Application.Quit();
    }
}
