@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET cmdTimes=%1
SET startDelay=%2
SET waitTime=%3
SET waitCount=%4
SET restartDelay=%5

SET portMin=49152
SET portMax=65535 
SET copyNo=0
SET restart=-1

IF %restartDelay% GEQ 0 (
	SET /A "restart=startDelay+(waitTime*waitCount)+restartDelay"
)

SET count=0
FOR /L %%z IN (0,0,1) DO (

	SET /A "start = %portMin% + (%cmdTimes% * !count!)"
	SET /A "end = !start! + %cmdTimes% - 1"
	IF !end! GTR %portMax% (
		START "" "run.bat" %1 %2 %3 %4 %5
		EXIT
	)
	
	SET /A "offsetS = !TIME:~6,2! + %startDelay%"
	SET /A "offsetM = !TIME:~3,2!"
	SET /A "offsetH = !TIME:~0,2!"
	
	IF !offsetS! GEQ 60 (
		SET /A "offsetM = !offsetM! + (!offsetS! / 60)" 
		SET /A "offsetS = !offsetS! %% 60"
	)
	IF !offsetM! GEQ 60 (
		SET /A "offsetH = !offsetH! + (!offsetM! / 60)"
		SET /A "offsetM = !offsetM! %% 60"
	)
	IF !offsetH! GEQ 24 (
		SET /A "offsetH = 0"
	)
	
	ECHO:
	ECHO NOW:   !TIME:~0,2! !TIME:~3,2! !TIME:~6,2!
	ECHO START: !offsetH! !offsetM! !offsetS!
	ECHO PORTS: !START! - !end!
	
	FOR /L %%j IN (!start!,1,!end!) DO (
		START "" MasterServerClient.exe -batchmode -nographics -logFile "MasterServerClient_Data/Output_%%j.log" -framerate 15 -port %%j -starttime !offsetH! !offsetM! !offsetS! -waitseconds %waitTime%
	)
	
	IF %restart% LEQ 0 (
		EXIT
	) else (
		TIMEOUT /T %restart% /NOBREAK
	)
	
	SET /A "count = !count! + 1"
)