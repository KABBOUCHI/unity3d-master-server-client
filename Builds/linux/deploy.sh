#! /bin/bash

echo $(hostname): $(tar -vczf master-server-client.tar.gz master-server-client/ | wc -l) files compressed
scp master-server-client.tar.gz $1:.
ssh $1 'echo "$(hostname)": "$(tar -vxzf master-server-client.tar.gz | wc -l)" files extracted'
