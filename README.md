### Unity3D Master Server Client ###

The client for interfacing [Master Server][MSRepo].

Master Server: A centralised server for accepting and distributing available game servers.

### Configuration ###

| Field                            | Description                                                                                                    |
| :------------------------------- | :------------------------------------------------------------------------------------------------------------- |
| **Master Server**                |                                                                                                                |
| Master Server URL                | The URL for the master server.                                                                                 |
| Master Server Port               | The port for the master server.                                                                                |
| **Created Socket Configuration** |                                                                                                                |
| Network Address                  | The network address that the master server client will bind to and listen on. (Optional)                       |
| Network Port                     | The network port that the master server client will bind to and listen on.                                     |
| Global Config                    | The Unity3D's Network configuration.                                                                           |
| Connection Config                | The Unity3D's connection/socket configuration.                                                                 |
| Channel ID                       | The channel ID that the master server client has exclusive use of.                                             |
| Max Connections                  | The amount of connections that can be open simultaneously.                                                     |
| Msg Buff Size                    | The size of the buffer used for receiving messages.                                                            |
| Max Net Events Per Frame         | The maximum amount of network messages to proccess before the next frame is allowed to render.                 |
| **LAN Discovery Configuration**  |                                                                                                                |
| LAN Discovery Port               | The port that LAN servers will be sent to. (Will only receive when "Network Port" equals "LAN Discovery Port") |
| LAN Discovery Timeout            | How often the LAN server will be broadcaster to the LAN.                                                       |
| LAN Discovery Key                | A unique key for this application.                                                                             |
| **Subroutine Frequencies**       |                                                                                                                |
| Idle Connection Check            | The frequency in seconds that the server checks if connections are idling (inactive).                          |
| Idle Time Disconnect             | The time in seconds a connection can be inactive for, before being classed as idling and disconnected.         |
| **Misc**                         |                                                                                                                |
| Max Outgoing MS Connections      | The maximum allowed simultaneous outgoing connections to the master server.                                    |
| Max Outgoing Other Connections   | The maximum allowed simultaneous outgoing connections to servers other than the master server.                 |
| Server Info Length               | The amount of bytes that can be sent from each game server.                                                    |

### Class Documentation ###

```
#!csharp
/// <summary>
/// Description of any errors or warnings that have occurred in this class.
/// </summary>
public string ErrorDescription;

/// <summary>
/// Uses the DNS to retrieve the IPv6 string of the given URL or null on error.
/// </summary>
/// <returns>The IPv6 of the given URL or null on error</returns>
public static string GetIPv6(string url);


/******************************************* Master Server References ********************************************/
public string MasterServerURL;
public int MasterServerPort;


/**************************************** Master Server Client References ****************************************/
/// <summary>
/// Will not set if IsInitialised is true.
/// </summary>
public string NetworkAddress;

/// <summary>
/// Will not set if IsInitialised is true.
/// </summary>
public UInt16 NetworkPort;

/// <summary>
/// Will not set if IsInitialised is true.
/// </summary>
public GlobalConfig GlobalConfig;

/// <summary>
/// Will not set if IsInitialised is true.
/// </summary>
public ConnectionConfig ConnectionConfig;

/// <summary>
/// Will not set if IsInitialised is true.
/// Should be the channel that the master server and client exclusively use to communicate.
/// </summary>
public byte ChannelID;

/// <summary>
/// Will not set if IsInitialised is true.
/// </summary>
public int MaxConnections;

public int HostID;
public UInt16 LANDiscoveryPort;
public int LANDiscoveryTimeout;

/// <summary>
/// HasSet of unique LAN ServerID's populated by a <see cref="NetworkEventType.BroadcastEvent"/> event with the internal socket.
/// <see cref="LANServerIDs.Clear()"/> may need to be called to make sure ServerIDs are current.
/// </summary>
public HashSet<ServerID> LANServerIDs;

/// <summary>
/// <see cref="NetworkMessageDelegate"/> for a <see cref="UnityEngine.Networking.MsgType.Connect"/> handler when using this class with an external socket,
/// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
/// </summary>
public NetworkMessageDelegate OnConnectDelegates;

/// <summary>
/// <see cref="NetworkMessageDelegate"/> for a <see cref="UnityEngine.Networking.MsgType.Disconnect"/> handler when using this class with an external socket,
/// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
/// </summary>
public NetworkMessageDelegate OnDisconnectDelegates;

/// <summary>
/// <see cref="NetworkMessageDelegate"/> for a <see cref="MsgType.MasterServerData"/> handler when using this class with an external socket,
/// i.e. the socket added with <see cref="Init(int, byte, ushort, out string)"/>. 
/// </summary>
public NetworkMessageDelegate OnDataDelegates;

/// <summary>
/// If this class has been initialised or not.
/// </summary>
public bool IsInitialised;

/// <summary>
/// Creates a socket for this class to use (using ConnectionConfig, MaxConnections and NetworkAddress:NetworkPort) and Initialises the class.
/// Pre: Channel should be set to a channel in ConnectionConfig that it can have exclusive use of.
/// </summary>
/// <param name="errorDesc">A description of any error that occurred.</param>
/// <returns>success or failure.</returns>
public bool Init(out string errorDesc);

/// <summary>
/// Sets up the class to use the given socket and Initialises the class.
/// </summary>
/// <param name="hostID">The hostID/socket for this class to use.</param>
/// <param name="channelID">The channel for this class to have exclusive use of.</param>
/// <param name="port">The port that the given socket is bound to.</param>
/// <param name="errorDesc">A description of any error that occurred.</param>
/// <returns>success or failure.</returns>
public bool Init(int hostID, byte channelID, UInt16 port, out string errorDesc);

/// <summary>
/// Shuts down the class and utilised sockets so it may be reinitialised.
/// Should be used as a last resort, for a graceful shut down <see cref="GracefulShutdown"/>
/// </summary>
/// <param name="errorDesc">A description of any error that occurred.</param>
/// <param name="ShutdownNetworkTransport">Leaves the NetworkTransport layer and sockets intact for external use.</param>
/// <returns>success.</returns>
public bool Shutdown(out string errorDesc, bool ShutdownNetworkTransport = true);

/// <summary>
/// Stops the server and shuts down the class and utilised sockets so it may be reinitialised.
/// </summary>
/// <param name="onReturn">(bool) Weather server shut down gracefully, (string) A description of any error that occurred or information.</param>
/// <param name="ShutdownNetworkTransport">Leaves the NetworkTransport layer and sockets intact for external use.</param>
/// <returns></returns>
public IEnumerator GracefulShutdown(Action<bool, string> onReturn, bool ShutdownNetworkTransport = true);


/********************************************* Server References ********************************************/
/// <summary>
/// Info or data about your game server.
/// </summary>
public byte[] ServerInfo;

/// <summary>
/// Size of ServerInfo.
/// </summary>
public int ServerInfoLength;

/// <summary>
/// Returns if the server has been sent to the Master Server and the corresponding public ServerID if one exists.
/// </summary>
/// <param name="serverID">The public server ID or default(ServerID) if none exists.</param>
/// <returns>ServerPublic</returns>
public bool PublicServerID(out ServerID serverID);

/// <summary>
/// If the server has been sent to the Master Server for everyone to see.
/// </summary>
public bool ServerPublic;

/// <summary>
/// Set to true by ServerStarted() and false by ServerStopped().
/// </summary>
public bool ServerRunning;

/// <summary>
/// Using ServerInfo and ServerPort sets up a server listing to be found and posted to the Master Server if ServerPublic is true.
/// </summary>
/// <param name="makePublic">Weather to make this server public or not.</param>
/// <param name="onReturn">(bool) Weather server started successfully, (string) A description of any error that occurred or information.</param>
public IEnumerator ServerStarted(bool makePublic, Action<bool, string> onReturn);

/// <summary>
/// Using the given info and port sets up a server listing to be found and posted to the Master Server if ServerPublic is true.
/// </summary>
/// <param name="info">The information or data about the game server starting.</param>
/// <param name="makePublic">Weather to make this server public or not.</param>
/// <param name="onReturn">(bool) Weather server started successfully, (string) A description of any error that occurred or information.</param>
public IEnumerator ServerStarted(byte[] info, bool makePublic, Action<bool, string> onReturn);

/// <summary>
/// Stops the server listing from being found.
/// </summary>
/// <param name="onReturn">(bool) Weather server stopped successfully, (string) A description of any error that occurred.</param>
public IEnumerator ServerStopped(Action<bool, string> onReturn);


/******************************************** Connection Functionality *******************************************/
/// <summary>
/// Uses MasterServerClient functionality to asynchronously connect to the given server 
/// then sets up the connection for general server communication by sending serverMessage on serverChannel.
/// </summary>
/// <param name="address">The address to connect to.</param>
/// <param name="port">The port to connect to.</param>
/// <param name="serverChannel">The channel that the server uses other than ChannelID.</param>
/// <param name="serverMessage">Data that the server may use to identify the new connection.</param>
/// <param name="onReturn">(int) Connection ID on success 0 otherwise. (string) A description of any error that occurred</param>
public IEnumerator ConnectToServer(string address, UInt16 port, byte serverChannel, byte[] serverMessage, Action<int, string> onReturn);


/**************************************** Game Server Info Functionality *****************************************/
/// <summary>
/// Retrieves the information about the server at the given socket.
/// </summary>
/// <param name="address">The server address to retrieve the info from.</param>
/// <param name="port">The server port to retrieve the info from.</param>
/// <param name="onReturn">(bool) weather the server info was retrieved successfully or not, (string) A description of any error that occurred.</param>
/// <param name="result">(byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
/// <returns></returns>
public IEnumerator GetServerInfo(string address, UInt16 port, Action<bool, string> onReturn, Action<byte[], int> result);

/// <summary>
/// Gets the successfully retrieved server listings
/// </summary>
/// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
/// <param name="onError">(string) A description of the error that occurred.</param>
public void GetServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null);

/// <summary>
/// Using <see cref="LANServerIDs"/> gets the successfully retrieved server listings.
/// </summary>
/// <param name="newListing">(string) The address of the server, (ushort) The port of the server, (byte[]) The servers info, (int) The delay in ms to receive the info after it was sent.</param>
/// <param name="onError">(string) A description of the error that occurred.</param>
public void GetLANServerListings(Action<string, UInt16, byte[], int> newListing, Action<string> onError = null);


/************************************** Master Server ServerID Functionality *************************************/
/// <summary>
/// Posts the server ID to the master server. (attempts connection if MSConID is less than or equal to 0).
/// </summary>
/// <param name="mSConID">The connection ID for the connection to the master server or 0 to internally connect</param>
/// <param name="onReturn">(bool) if server ID was posted successfully. (string) A description of any error that occurred or general message</param>
/// <returns></returns>
public IEnumerator PostServerID(int mSConID, Action<bool, string> onReturn);

/// <summary>
/// Removes any posted server ID. Server should be removed before closing the program.
/// </summary>
/// <param name="onReturn">(Bool) Weather the ServerID was successfully removed or not, (string) A description of any error that occurred.</param>
/// <returns></returns>
public IEnumerator RemoveServerID(Action<bool, string> onReturn);

/// <summary>
/// Gets a complete list of unique ServerIDs from the master server.
/// </summary>
/// <param name="onReturn">(int) The count of ServerID's retrieved and -1 on error, (string) A friendly description of any error that occurred</param>
/// <param name="newServerID">(ServerID) A new ServerID that has successfully been retrieved.</param>
/// <returns></returns>
public IEnumerator GetServerIDs(Action<int, string> onReturn, Action<ServerID> newServerID);
```

[MSRepo]: https://bitbucket.org/gerardryan/unity3d-master-server  "Master Server Repository"

### License ###

All file in this project are available under the MIT License (MIT) as follows:

Copyright (c) 2017 Gerard Ryan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.